from Player import Player
from Plot import Plot
import random

class Board:
	BoardWidth = 10
	BoardHeight = 10
	Plots = []
	def __init__(self,NumPlayers):	
		Board.BoardWidth=NumPlayers
		Board.BoardHeight = NumPlayers
		for x in range(0,Board.BoardWidth):
			Board.Plots.append(list())
			for y in range(0,Board.BoardHeight):
				self.Plots[x].append(Plot(x,y))

	def GetHeight(self):
		return self.BoardHeight

	def GetWidth(self):
		return self.BoardWidth
		
	def GetPlot(self,x,y):
		return Plots[x][y]
	
	def GetRandomPlot(self):
		owner = 99
		count = 20
		while owner!=None and count>0:
			x=random.randrange(0,self.BoardWidth)
			y=random.randrange(0,self.BoardHeight)
			owner = self.Plots[x][y].Owner
			count+=-1
		if count < 1: 
			raise NoUnownedPlotsException ('No Unowned Plots Left in Get Random Plot')
		else: 
			return self.Plots[x][y]
	
	def CalcValueAdd(self,ThisPlot):
		TotalValueAdd = 0
		MyX=ThisPlot.Address[0]
		MyY=ThisPlot.Address[1]
		for x in range(0,Board.BoardWidth):
			for y in range(0,Board.BoardHeight):
				testPlot = self.Plots[x][y]
				MDistance = abs(x-MyX)+abs(y-MyY)
				if testPlot != ThisPlot:
					TotalValueAdd += float(testPlot.Investments + testPlot.InitialCost)/float(MDistance)
		ThisPlot.ValueAdd=TotalValueAdd
		return TotalValueAdd		

	
def PrintPlots():
	for x in range(0,Board.BoardWidth):
		for y in range(0,Board.BoardHeight):
			Board.Plots[x][y].PrintPlot()

def DisplayPlayerCity():
	for x in range(0,Board.BoardWidth):
		for y in range(0,Board.BoardHeight):
			Owner=Board.Plots[x][y].Owner
			if Owner==None:
				PlayerChar = ' .'
			else:
				PlayerChar = "{0:>2}".format(str(Owner.PlayerNo))
			print PlayerChar,
		print

def DisplayValueCity():
	for x in range(0,Board.BoardWidth):
		for y in range(0,Board.BoardHeight):
			Plot = Board.Plots[x][y]
			Value=Plot.InitialCost+Plot.Investments
#			if Value>9:
#				ValueChar = '@'
#			else:
#				ValueChar = str(Value)
			print "{0:>4}".format(int(Value)),
		print

def GetPlotList():
	return Board.Plots
import Player
import Board

PlayerNames = ['Dave','Keith','Dorita','Janine','Scott','Sabrina','Mathew','Kira','Matt','Leslie','Jeanne','Carl','Peter','Mary','Paul','Lee','Iren','Eric','Fluffy']

NUMPLAYERS = 11 #how many players
STARTING_PLOTS = NUMPLAYERS/3 #how many plots do players get initially
STARTING_MONEY = NUMPLAYERS*2 #how much money do people get initially
MAX_TURNS = 10 #how much
VALUE_ADD_FRACTION = 0.1 #what fraction of the total surrounding plots do you get
REVENUE_FRACTION = 0.2 #what fraction of your total networth do you get

Turn = 0

#create board
MyBoard = Board.Board(NUMPLAYERS)
#create players
PlayerList=[]
for i in range(0,NUMPLAYERS):
	thisPlayer = Player.Player(PlayerNames[i])
	for j in range(0,STARTING_PLOTS):
		thisPlot = MyBoard.GetRandomPlot()
		thisPlayer.AddPlot(thisPlot)
		thisPlot.SetOwner(thisPlayer)
	PlayerList.append(thisPlayer)
item = PlayerList[0]

#print initial game info
Player.PrintAllPlayers()
#Board.PrintPlots()

print "Manhattan Turn #",Turn
Turn+=1
print "Players"
Board.DisplayPlayerCity()
print
print "Values"
Board.DisplayValueCity()

# we gotta do this before the game starts
Player.SetPlayerOrder()

while Turn<=MAX_TURNS:
#Buy land -- players request land and then 
	LandRequests = []
	PlayerList =  Player.GetPlayerList()
	for pl in PlayerList:
		LandRequests.append(pl.PlayerBuyLand())
	# for starters we know players don't buy land so we'll ignore this phase for now

#Invest
	PlayerList =  Player.GetPlayerList()
	for pl in PlayerList:
		money = pl.Cash
		invest = money/3
		if invest<1:
			continue
		for plot in pl.Plots:
			plot.AddInvestment(invest)
			pl.SubCash(invest)

#Revenue
	PlayerList =  Player.GetPlayerList()
	for pl in PlayerList:
		playerValueAdd=0
		playerNetWorth=0
		for plot in pl.Plots:
			playerValueAdd += MyBoard.CalcValueAdd(plot)
			playerNetWorth += plot.Investments + plot.InitialCost
		revenue = (float(playerNetWorth)+float(playerValueAdd)*VALUE_ADD_FRACTION)*REVENUE_FRACTION
		pl.AddCash(revenue)
#turn order
	print "\nEnd of Manhattan Turn #",Turn
	print "Players"
	Player.PrintAllPlayers()
	Board.DisplayPlayerCity()
	print
	print "Values"
	Board.DisplayValueCity()
	Turn+=1	
#print "++++++++++++++++++++++++++++++++++++++++++"	
#list = Board.GetPlotList()
#print list
import AI
class Player:
	Players = []
	PlayerOrder=[]
	
	def __init__(self,name,AIType,cash=20):
		self.Cash = cash
		self.Name = name
		self.Plots = []
		self.PlayerNo = len(Player.Players)
		Player.Players.append(self)
		self.Strategy = AI(AIType)
	
	def AddPlot	(self,PlotNum):
		self.Plots.append(PlotNum)

	def RemovePlot (self,PlotNum):
		try:
			self.Plots.remove(PlotNum)
		except:
			pass # if it's not there, then we've removed it; no need to make a big deal
		
	def AddCash(self,Cash):
		self.Cash += Cash

	def SubCash(self,Cash):
		self.Cash += -Cash

	def GetCash(self):
		return self.Cash

	def GetPlots (self):
		return self.Plots

	def GetName(self):
		return self.Name

	def PrintPlayer(self):
		networth = 0
		for p in self.Plots:
			networth+=p.InitialCost + p.Investments + float(p.ValueAdd)*.1
		print self.Name,self.Cash,networth
		for p in self.Plots:
			p.PrintPlot(),
		print
	
	def PlayerBuyLand(self):
		return [] #players never buy land

def SetPlayerOrder():
	#this is supposed to be based on the least IV first with a die roll for ties.
	for p in Player.Players:
		Player.PlayerOrder.append(p)

def PrintAllPlayers():
	for p in Player.Players:
		p.PrintPlayer()
		print "---------------------\n"
	
def GetPlayerName(PlayerNo):
		return Player.Players[PlayerNo].Name
def GetPlayerList():
	return Player.Players
def GetPlayerOrder():
	return Player.PlayerOrder
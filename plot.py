import Player
class Plot:
	def __init__(self,x,y,InitialCost=1):
		self.InitialCost=InitialCost
		self.Investments = 0
		self.Owner = None
		self.Address = [x,y]
		self.ValueAdd = 0 #calculated every turn
	
	def PlotCost(self):
		return self.InitialCost+Investments
	
	def AddInvestment(self,Amount):
		self.Investments += Amount
	
	def GetOwner(self):
		return self.Owner
	
	def SetOwner(self,NewOwner):
		self.Owner = NewOwner
	
	def GetInvestment(self):
		return self.Investments
	
	def PrintPlot(self):
		if self.Owner == None:
			OwnerName = "Unowned"
		else:
			OwnerName = self.Owner.Name
		print self.Address,OwnerName, self.InitialCost,self.Investments
